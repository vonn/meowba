﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameTimerController : MonoBehaviour {
	public Text timer;
	public double gameStart = 0;
	bool running = true;


	// Update is called once per frame
	void Update () {
		if(running)
		{
			int seconds = (int)(Time.time - gameStart) % 60;
			int minutes = (int)(Time.time - gameStart) / 60;
			string min, sec;
			if(seconds < 10)
				sec = "0" + seconds;
			else
				sec = "" + seconds;
			if(minutes < 10)
				min = "0" + minutes;
			else
				min = "" + minutes;
			timer.text = min + ":" + sec;
		}
	}

	public void Stop() 
	{
		running = false;
	}
}
