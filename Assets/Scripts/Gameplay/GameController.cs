using UnityEngine;
using System.Collections;
using System.Collections.ObjectModel;

public class GameController : MonoBehaviour
{

    #region Static Vars
    static GameController m_instance = null;
    /// <summary>
    /// GameController singleton.
    /// </summary>
    /// <value>The m_instance.</value>
    public static GameController instance
    {
        get { return m_instance; }
        private set { m_instance = value; }
    }
    #endregion

    #region Member Vars
    public PlayerList Players { get; private set; }
    public Transform Team1Object; // folder of objects in team 1
    public Transform Team2Object; // folder of objects in team 2

    public UIController ui;

    public PlayerController me;
    public PlayerController pc1, pc2;
    public GameTimerController timer;

    public bool matchRunning;
    #endregion

    void Awake()
    {
        // manage singleton business
        if (m_instance == null)
        {
            m_instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (m_instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        // set up fresh players list
        Players = new PlayerList();

        // set up the team objects
        Team1Object = (Transform)GameObject.Find("Team 1").transform;
        Team2Object = (Transform)GameObject.Find("Team 2").transform;
    }

    void Start()
    {
        // TODO this logic shouldn't be in gamecontroller
        /*if (player == null) return;
	
        //Filler Player Character
        //REMOVE THIS
        player.Attributes.character = "Player";
        ui.CreateHealthBar(player);*/

        // TODO a real start-of-game thing (countdown, announcements, etc)
        matchRunning = true;
    }

    void Update()
    {
        //  -- DEBUG CHEAT BUTTONS -- 
        // F1
        if (Input.GetKeyDown(KeyCode.F1))
        {
            // TEAM 1 WIN
            GameOver((int)Team.ONE);
        }
        // F2
        if (Input.GetKeyDown(KeyCode.F2))
        {
            // TEAM 2 WIN
            GameOver((int)Team.TWO);
        }
    }

    void OnLevelWasLoaded(int level)
    {
        // if the arena scene was loaded
        if (Application.loadedLevelName == Scenes.Arena)
        {
            // hide the chat TODO maybe somewhere else?
            Chat.instance.gameObject.SetActive(false);

            //start the timer
            timer.gameStart = Time.time;

            // specific things for network user
            if (Network.player == NetworkManager.instance.hostPlayer.NPlayer)
            {
                // MY PLAYER IS PURPLE
                GameController.instance.SetupPlayer(1);
            }
            // specific things for opponent
            else
            {
                // MY PLAYER IS YELLOW
                GameController.instance.SetupPlayer(2);
            }
        }

    }

    /// <summary>
    /// Sets up the Player 1/2 prefabs for the player depending on whom they should be
    /// </summary>
    /// <param name="playerNum"></param>
    public void SetupPlayer(int playerNum)
    {
        // SET UP PLAYER 1 (HOST)
        pc1 = GameController.instance.Team1Object.FindChild("Player 1").GetComponent<PlayerController>();
        GameController.instance.Players.Add(pc1);

        // SET UP PLAYER 2 (OPPONENT)
        pc2 = GameController.instance.Team2Object.FindChild("Player 2").GetComponent<PlayerController>();
        GameController.instance.Players.Add(pc2);

        // Determine who I am (me) 

        // setup player 1 (host)
        if (playerNum == 1)
        {
            me = pc1;
            // disable p2's input
            pc2.GetComponent<PlayerInput>().enabled = false;
        }
        // setup player 2 (opponent)
        else if (playerNum == 2)
        {
            me = pc2;
            // disable p1's input
            pc1.GetComponent<PlayerInput>().enabled = false;

            // ask the server to assign this netview's ID = mine
            //var id = Network.AllocateViewID();
            //NetworkManager.instance.networkView.RPC("AssignPlayer2NV", RPCMode.Server, new object[] { id });
        }
        else
        {
            me = null;
        }
        // set the UI controller to consider me its master
        ui.Owner = me;

        // set target in rts cam
        Camera.main.GetComponent<RTSCamera>().target = me;

        // get the UI to create a healthbar for it
        //ui.CreateHealthBar(me);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="winningTeam"></param>
    internal void GameOver(int winningTeam)
    {
        // stop timer
        timer.Stop();
        matchRunning = false;

        // pause everything
        // HACK just slow everything for now
        Time.timeScale *= .5f;

        // team wins

        // TODO show panel on how wins
        // show victory
        if (me._team == (Team)winningTeam)
        {
            print("YOU WIN");
        }
        // show defeat
        else
        {
            print("YOU LOSE");
        }

        // calculate things (points, etc.)

        // return to lobby? in 5 sec
        Invoke("ReturnToLobby", 5);
    }

    void ReturnToLobby()
    {
        // reset timescale haha
        Time.timeScale = 1;

        // disconnect from w/e
        //Network.Disconnect();

        // TODO go to lobby instead, but couldn't get lobby to work again so easily after a game
        MainMenuController.RequestMenuChange(MenuState.MAIN);

        // FINALLY DESTROY THIS?
        Destroy(gameObject);
    }
}
