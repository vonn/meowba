﻿using System.Runtime.Serialization;

/// <summary>
/// The attributes of a PvPAvatar
/// </summary>
[System.Serializable]
public sealed class PvPAttributes : ISerializable
{
    #region MEMBER VARS
    public int level = 1;
    public int exp;
    public int expToLevel;
    public int expValue;
    public int health;
    public int mana;
    public int currHealth;
    public int currMana;
    public float runSpd;
    // primary battle stats
    public int spellPwr;
    public int atkPwr;
    // secondary battle stats
    public float atkSpd = 1; // attacks per second
    public float lifeSteal; // percent physical dmg stolen as HP
    public float spellVamp; // percent spell dmg stolen as HP
    public float spellPen; // percent of spellResist to ignore
    public float armorPen; // perfect of armor to ignore
    public float cdReduction; // percent to reduce ability CD by
    // hidden stats
    public float atkRange = 1.3f; // melee range
    // defensive stats
    public int armor = 5; // percent of physical dmg to reduce
    public int spellResist = 5; // percent of spell dmg to reduce
    #endregion

    /// <summary>
    /// Empty Constructor
    /// </summary>
    public PvPAttributes()
    {

    }

    /// <summary>
    /// Serializes the class into info
    /// </summary>
    /// <param name="info"></param>
    /// <param name="context"></param>
    void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
    {
        // info
        info.AddValue("level", level, typeof(int));
        info.AddValue("exp", exp, typeof(int));
        info.AddValue("expToLevel", expToLevel, typeof(int));
        info.AddValue("expValue", expValue, typeof(int));
        info.AddValue("health", health, typeof(int));
        info.AddValue("mana", mana, typeof(int));
        info.AddValue("currHealth", currHealth, typeof(int));
        info.AddValue("currMana", currMana, typeof(int));
        // primary
        info.AddValue("spellPwr", spellPwr, typeof(int));
        info.AddValue("atkPwr", atkPwr, typeof(int));
        //secondary
        info.AddValue("atkSpd", atkSpd, typeof(float));
        info.AddValue("lifeSteal", lifeSteal, typeof(float));
        info.AddValue("spellVamp", spellVamp, typeof(float));
        info.AddValue("spellPen", spellPen, typeof(float));
        info.AddValue("armorPen", armorPen, typeof(float));
        info.AddValue("cdReduction", cdReduction, typeof(float));
        // hidden
        info.AddValue("atkRange", atkRange, typeof(float));
        // defensive
        info.AddValue("armor", armor, typeof(int));
        info.AddValue("spellResist", spellResist, typeof(int));
    }

    /// <summary>
    /// The special constructor is used to deserialize values. 
    /// </summary>
    /// <param name="info"></param>
    /// <param name="context"></param>
    public PvPAttributes(SerializationInfo info, StreamingContext context)
    {
        // info
        level = (int)info.GetValue("level", typeof(int));
        exp = (int)info.GetValue("exp", typeof(int));
        expToLevel = (int)info.GetValue("expToLevel", typeof(int));
        expValue = (int)info.GetValue("expValue", typeof(int));
        health = (int)info.GetValue("health", typeof(int));
        mana = (int)info.GetValue("mana", typeof(int));
        currHealth = (int)info.GetValue("currHealth", typeof(int));
        currMana = (int)info.GetValue("currMana", typeof(int));
        // primary
        spellPwr = (int)info.GetValue("spellPwr", typeof(int));
        atkPwr = (int)info.GetValue("atkPwr", typeof(int));
        //secondary
        atkSpd = (float)info.GetValue("atkSpd", typeof(float));
        lifeSteal = (float)info.GetValue("lifeSteal", typeof(float));
        spellVamp = (float)info.GetValue("spellVamp", typeof(float));
        spellPen = (float)info.GetValue("spellPen", typeof(float));
        armorPen = (float)info.GetValue("armorPen", typeof(float));
        cdReduction = (float)info.GetValue("cdReduction", typeof(float));
        // hidden
        atkRange = (float)info.GetValue("atkRange", typeof(float));
        // defensive
        armor = (int)info.GetValue("armor", typeof(int));
        spellResist = (int)info.GetValue("spellResist", typeof(int));
    }
}