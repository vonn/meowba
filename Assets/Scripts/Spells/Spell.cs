﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SpellTarget))]
[RequireComponent(typeof(SpellCaster))]
[RequireComponent(typeof(SpellCost))]

/// <summary>
/// Primordial component of a Spell.
/// </summary>
public class Spell : MonoBehaviour {

	#region Member Properties
	public string SpellName;
	
	[SerializeField] SpellTarget _tar;
	public SpellTarget Target { 
		get { return _tar; } 
		set { _tar = value; } 
	}
	
	[SerializeField] SpellCaster _caster;
	public SpellCaster Caster { 
		get { return _caster; } 
		set { _caster = value; } 
	}
	
	[SerializeField] SpellCost _cost;
	public SpellCost Cost {
		get { return _cost; }
		set { _cost = value; }
	}
	
	[SerializeField] SpellEffect[] _effects;
	public SpellEffect[] Effects { 
		get { return _effects; }
	}
	#endregion
	
	void Update() {
		
	}
	
	[RPC]
	public void CastTargeted(PvPAvatar caster, PvPAvatar target) {
		Target.Type = SpellTargetType.SINGLE;
		Target.Target = target;
		Cast(caster);
	}
	
	[RPC]
	public void CastArea(PvPAvatar caster, Vector3 origin) {
		Target.Type = SpellTargetType.AREA;
		Cast(caster);
	}
	
	public void Cast(PvPAvatar caster) {	
		Caster.Caster = caster;
		
		// Update the nextCanCast
		Caster.ApplyCooldown();
		Caster.SubtractCost();
		
		// TODO handle other spell types
		switch (Target.Type) {
		
		// Single target spell
		case SpellTargetType.SINGLE:
			StartCoroutine(CastSingle());
			break;
		}
	}
	
	/// <summary>
	/// Handles a spell being cast at a single target
	/// </summary>
	public IEnumerator CastSingle() {
		AvatarState prevState = Caster.Caster._state;
		
		// Only cast from idle
		if (prevState != AvatarState.IDLE) yield break;
		// Change to casting state
		Caster.Caster._state = AvatarState.CASTING;
		Caster.Caster._navAgent.Stop(true);
		
		// arbitrary wait for cast time 
		// TODO show castbar here
		yield return new WaitForSeconds(Caster.CastTime);
		
		// Now that cast time is over, do the effects (sequentially)
		for (int i = 0; i < _effects.Length; i++) {
			_effects[i].Effect();
		}
		
		Caster.Caster._state = prevState;
		Caster.Caster._navAgent.Resume();
	}
}