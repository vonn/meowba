﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Spell target type.
/// </summary>
public enum SpellTargetType { NONE, SINGLE, AREA, CONE }

/// <summary>
/// Information about the target of a spell
/// </summary>
public class SpellTarget : MonoBehaviour {

	Spell thisSpell;
	public PvPAvatar Target;
	public bool RequireLivingTarget = true;
	public bool TargetHostile = true;
	public SpellTargetType Type = SpellTargetType.SINGLE;
	public bool Valid;
	
	void Awake() {
		thisSpell = GetComponent<Spell>();
	}
	
	void Update() {
		switch (Type) {
		// TODO handle area/cone targeting
		case SpellTargetType.AREA:
			Valid = false;
			break;
		case SpellTargetType.CONE:
			Valid = false;
			break;
		case SpellTargetType.NONE:
			Valid = true;
			break;
		case SpellTargetType.SINGLE: 	
				
			Valid = (RequireLivingTarget && Target._state != AvatarState.DEAD) && // is it alive
				(
					(TargetHostile && thisSpell.Caster.Caster._team != Target._team) || // am i targetting hostile
					(!TargetHostile && thisSpell.Caster.Caster._team == Target._team)   // am i targetng friendly
				);
				
			break;
		}
	}
}
