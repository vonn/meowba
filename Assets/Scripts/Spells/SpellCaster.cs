﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Information regarding the caster and how the spell can be cast
/// </summary>
public class SpellCaster : MonoBehaviour {

	Spell thisSpell;
	public PvPAvatar Caster;
	public bool RequireLivingCaster = true;
	
	public float CastTime = .5f;
	public float Cooldown = 6f;
	public float CastRange = 1.0f;
	public float NextCanCast = 0;
	// context-sensitive floats
	public float MissleSpeed = 1.0f; // for SINGLE
	public float AreaRadius = 1.0f;  // for AREA
	
	public bool canCast;
	
	void Awake() {
		thisSpell = GetComponent<Spell>();
	}
	
	void Update() {
		// COULD I CAST LAST FRAME
		bool canCastLastFrame = canCast;
	
		// CAN I CAST NOW
		canCast = Time.time > NextCanCast && thisSpell.Cost.Available && thisSpell.Target.Valid;
		
		// DID I JUST GAIN THE ABILITY TO CAST
		if (!canCastLastFrame && canCast) {
			// TODO notify player that they can cast now
			print (thisSpell.SpellName + " is ready to cast!");
		}
	}
	
	public void ApplyCooldown() {
		NextCanCast = Time.time + Cooldown;
	}
	
	public void SubtractCost() {
		Caster.Attributes.currMana -= thisSpell.Cost.Cost;
		// OUT OF MANA
		if (Caster.Attributes.currMana < 0) {
			throw new UnityException("MY MANA SHOULDN'T GO NEGATIVE");
		}
	}
}
