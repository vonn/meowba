﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Ska-doosh~
/// </summary>
public class EffectDamage : SpellEffect
{

    /// <summary>
    /// The amoutn of damage to be dealt
    /// </summary>
    /// <value>The value.</value>
    public int Value;
    public float atkRatio;
    public float spellRatio;

    public void Awake()
    {
        Value += (int)atkRatio * GetComponent<SpellCaster>().Caster._attr.atkPwr;
        Value += (int)spellRatio * GetComponent<SpellCaster>().Caster._attr.spellPwr;
    }

    #region SpellEffect implementation
    public override void Effect()
    {
        // the target
        PvPAvatar tar = GetComponent<SpellTarget>().Target;
        // this spell
        Spell thisSpell = GetComponent<Spell>();

        // TODO: reimplement 
        //tar.TakeDamageSpell(Value, thisSpell);

        tar.TakeDamage(Value, (int)DamageType.SPELL, -1);

        // TODO visual fx
    }
    #endregion
}
