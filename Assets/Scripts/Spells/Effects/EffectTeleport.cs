﻿using UnityEngine;
using System.Collections;

public class EffectTeleport : SpellEffect {

	public Vector3 Destination;

	#region SpellEffect implementation
	public override void Effect()
	{
		// the target
		PvPAvatar caster = GetComponent<SpellCaster>().Caster;
		
		caster._trans.position = Destination;
		// TODO visual fx
	}
	#endregion
}
