﻿#define DEBUG
using UnityEngine;
using System.Collections;

/// <summary>
/// Avatar state.
/// </summary>
public enum AvatarState { IDLE, ATTACKING, CASTING, DEAD };
/// <summary>
/// Which team an avatar belongs to
/// </summary>
public enum Team { ONE, TWO, NEUTRAL };
/// <summary>
/// Avatar type.
/// </summary>
public enum AvatarType { PLAYER, MINION, BUILDING, TURRET };

[System.Serializable]
public class PvPAttributes {
    #region Combat Stats
	public string character; //hero name or minion type or building type
    public int level = 1;
    public int exp;
    public int expToLevel;
    public int health;
    public int mana;
    public int currHealth;
    public int currMana;
    public float runSpd;
    // primary battle stats
    public int spellPwr;
    public int atkPwr;
    // secondary battle stats
    public float atkSpd = 1; // attacks per second
    public float lifeSteal; // percent physical dmg stolen as HP
    public float spellVamp; // percent spell dmg stolen as HP
    public float spellPen; // percent of spellResist to ignore
    public float armorPen; // perfect of armor to ignore
    public float cdReduction; // percent to reduce ability CD by
    // hidden stats
    public float atkRange = 1.3f; // melee range
    // defensive stats
    public int armor = 5; // percent of physical dmg to reduce
    public int spellResist = 5; // percent of spell dmg to reduce
    #endregion
}

/// <summary>
/// A class that defines an avatar that can conduct pvp combat. That is,
/// minions, players, etc.
/// </summary>
[RequireComponent(typeof(NavMeshAgent))]
public class PvPAvatar : MonoBehaviour {
    #region MEMBER FIELDS
    protected NavMeshAgent _navAgent;
    protected Transform _trans;
    protected int _floorMask;
    [SerializeField] protected Team _team;
    [SerializeField] protected AvatarState _state = AvatarState.IDLE;

    [SerializeField] protected PvPAttributes _attr;
    /// <summary>
    /// Attributes (stats, values, etc) of the PvPAv.
    /// </summary>
    /// <value>The attributes.</value>
    public PvPAttributes Attributes {
        get { return _attr; }
    }

    protected AvatarType _type;
    public AvatarType Type {
        get { return _type; }
    }

    [SerializeField] protected float aggroRadius = 1.0f; // how close before i bite!
    protected Vector3 currentDestination; // where am i currently going?
    [SerializeField] protected bool lookForFight; // is my current move command an attack move?
    [SerializeField] protected bool stopped; // prevents from autoattacking
    [SerializeField] protected bool inCombat;
    [SerializeField] protected bool isPlayer;
    protected PvPAvatar currentTarget;
    bool isMoving;
    #endregion

    #region UNITY METHODS
    protected virtual void Awake() {
        _trans = transform;
        _floorMask = Layers2D.navGroundMask;
        _navAgent = GetComponent<NavMeshAgent>();
       // print ("pvpavatar started");
    }

    protected virtual void Initialize() {
        _attr = new PvPAttributes();
    }

    protected virtual void FixedUpdate() {
        // i has target to attack?
        if (currentTarget != null && _state == AvatarState.ATTACKING) {
            // Players and minions chase targets
            if (_type != AvatarType.TURRET) {
                // i only want to get close enough to be in attack range
                if (Vector3.Distance(_trans.position, currentTarget._trans.position) > _attr.atkRange) {
                    
                    SetDestination(currentTarget._trans.position);
                }
                else {
                    // TODO start auto attacking
                }
            }
            // buildings just make the shoot
            else {
                // should i shoot or nah
            }
        }
    }

    protected virtual void LateUpdate() {
        // have i reached my current destination?
        if (isMoving && Vector3.Distance(currentDestination, _trans.position) <= .005f) {
            isMoving = false;
            //print ("reached destination");
            lookForFight = true;
        }

        bool anyInRange = AggroCheck();
    }
    #endregion

    #region Target Handling
    /// <summary>
    /// Sets up a target to attack
    /// </summary>
    /// <param name="avatar">Avatar.</param>
    protected void SetTarget(PvPAvatar avatar) {
        lookForFight = false || _type == AvatarType.TURRET;
        currentTarget = avatar;

        // HOSTILE TARGET
        if (avatar._team != _team) {
            _state = AvatarState.ATTACKING;
        }
        // FRIENDLY TARGET
        else {

        }
    }

    /// <summary>
    /// Drop the current target
    /// </summary>
    protected void DropTarget() {
        currentTarget = null;
        _state = AvatarState.IDLE;
    }

	protected float DistToTarget() {
		if (currentTarget == null) return -1;

		return Vector3.Distance(_trans.position, currentTarget._trans.position);
	}

    /// <summary>
    /// Check within aggro radius for possible targets
    /// </summary>
    /// <returns><c>true</c>, if check was aggroed, <c>false</c> otherwise.</returns>
    protected bool AggroCheck() {
        if (!lookForFight) return false;
        
        Collider[] results = Physics.OverlapSphere(_trans.position, aggroRadius, Layers2D.pvpAvMask);
        bool anyHit = results.Length > 0;
        if (!anyHit) return false;
        
        Collider nearestCol = results[0];
        float nearestDistance = float.MaxValue;
        float distance;
        
        for (int i = 0; i < results.Length; i++) {
            Collider c = results[i];
            // ignore myself
            if (c.transform == _trans) continue;
            
            // is it the nearest one?
            distance = (_trans.position - c.transform.position).sqrMagnitude;
            if (distance < nearestDistance) {
                nearestDistance = distance;
                nearestCol = c;
            }
            else continue;
            
            PvPAvatar av = c.GetComponent<PvPAvatar>();
            //print(av.name);
            
            // make sure they are on different team
            if (av._team != _team) {
                // only hit neutrals if they are in combat
                if (av._team == Team.NEUTRAL && !av.inCombat) continue;
                
                print(_trans.name + " can attack " + av.name + " from " + av._team + " team.");
                SetTarget(av);
            }
        }
        
        return anyHit;
    }
    #endregion

    /// <summary>
    /// Wrapper func to set the nav's destination.
    /// </summary>
    /// <param name="dest">Destination.</param>
    protected void SetDestination(Vector3 newDest) {
        newDest.y = _trans.position.y;
        isMoving = true;
        currentDestination = newDest;
        _navAgent.SetDestination(currentDestination);
    }

#if DEBUG
    void OnDrawGizmos() {
        // Draw aggro sphere
        if (currentTarget == null) {
            Gizmos.color = lookForFight ? Color.red : Color.blue;
            Gizmos.DrawWireSphere(transform.position, aggroRadius);
        }
        // draw line to target
        else {
            Gizmos.color = currentTarget._team == _team ? Color.green : Color.red;
            Gizmos.DrawLine(_trans.position, currentTarget._trans.position);
        }
    }
#endif
}
