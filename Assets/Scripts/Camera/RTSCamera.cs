﻿/*
Taken/adapted from http://forum.unity3d.com/threads/rts-camera-script.72045/#post-465018
 */

using UnityEngine;
using System.Collections;

public class RTSCamera : MonoBehaviour
{

    public bool lockedOnTarget = true;

    // Can only follow pvp avs, otherwise we'd use Transform here
    [SerializeField]
    PvPAvatar _tar;
    public PvPAvatar target
    {
        get { return _tar; }
        set { _tar = value; }
    }

    public Vector3 targetOffset;

    public float distance = 5.0f;
    public float distanceMin = .5f;
    public float distanceMax = 15f;

    public float ScrollSpeed = 20f;
    public float ScrollEdge = 0.01f;

    private int HorizontalScroll = 1;
    private int VerticalScroll = 1;
    private int DiagonalScroll = 1;

    float PanSpeed = 10;

    Vector2 ZoomRange = new Vector2(-5, 5);
    float CurrentZoom = 0f;
    float ZoomZpeed = 1f;
    float ZoomRotation = 1f;

    private Vector3 InitPos;
    private Vector3 InitRotation;

    void Start()
    {
        //Instantiate(Arrow, Vector3.zero, Quaternion.identity);
        InitPos = transform.position;
        InitRotation = transform.eulerAngles;

    }

    void Update()
    {
        //PAN
        /*  if ( Input.GetKey("mouse 2") ) {
              //(Input.mousePosition.x - Screen.width * 0.5)/(Screen.width * 0.5)
            
              transform.Translate(Vector3.right * Time.deltaTime * PanSpeed * (Input.mousePosition.x - Screen.width * 0.5f)/(Screen.width * 0.5f), Space.World);
              transform.Translate(Vector3.forward * Time.deltaTime * PanSpeed * (Input.mousePosition.y - Screen.height * 0.5f)/(Screen.height * 0.5f), Space.World);  
          }
          else
          {*/

        // Press 'Y' to toggle locking/unlocking from target
        if (Input.GetKeyDown(KeyCode.Y))
        {
            lockedOnTarget = !lockedOnTarget;
        }

        Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
        //Vector3 position = negDistance + target.position;

        Rect screenRect = new Rect(0, 0, Screen.width, Screen.height);
        if ((!lockedOnTarget || (target && target._state == AvatarState.DEAD)) && screenRect.Contains(Input.mousePosition))
        {
            if (Input.GetKey(KeyCode.RightArrow) || Input.mousePosition.x >= Screen.width * (1 - ScrollEdge))
            {
                transform.Translate(Vector3.right * Time.deltaTime * ScrollSpeed, Space.World);
            }
            else if (Input.GetKey(KeyCode.LeftArrow) || Input.mousePosition.x <= Screen.width * ScrollEdge)
            {
                transform.Translate(Vector3.right * Time.deltaTime * -ScrollSpeed, Space.World);
            }

            if (Input.GetKey(KeyCode.UpArrow) || Input.mousePosition.y >= Screen.height * (1 - ScrollEdge))
            {
                transform.Translate(Vector3.forward * Time.deltaTime * ScrollSpeed, Space.World);
            }
            else if (Input.GetKey(KeyCode.DownArrow) || Input.mousePosition.y <= Screen.height * ScrollEdge)
            {
                transform.Translate(Vector3.forward * Time.deltaTime * -ScrollSpeed, Space.World);
            }
        }

        if (target && target._state != AvatarState.DEAD)
        {
            transform.position = target._trans.position + targetOffset/* + negDistance*/;
        }

    }

    void LateUpdate()
    {
        distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);
    }

}
