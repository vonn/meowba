﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HoverSelectionController : MonoBehaviour
{

    public UIController uiController;

    int rayLength = 100;

    [SerializeField]
    PvPAvatar previousTarget;
    [SerializeField]
    PvPAvatar target;

    void Update()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit floorHit;

        if (Physics.Raycast(camRay, out floorHit, rayLength, Layers.pvpAvMask))
        {
            target = floorHit.collider.gameObject.GetComponent<PvPAvatar>();

            // if we have new target
            if (target == null || !target.Equals(previousTarget))
            {
                // unhighlight previous target
                if (previousTarget != null)
                {
                    //previousTarget.renderer.material = previousMaterial;
                    previousTarget.renderer.material.SetColor("_OutlineColor", Color.black);
                }

                // set previous
                previousTarget = target;

                // highlight this bad boy

                // set up outline color
                Color col = GameController.instance.me.IsEnemy(target) ? Color.red : Color.green;
                // set the thing
                target.renderer.material.SetColor("_OutlineColor", col);
            }
        }
        else
        {
            if (previousTarget != null)
            {
                previousTarget.renderer.material.SetColor("_OutlineColor", Color.black);
            }

            target = null;
            previousTarget = null;
        }
    }

    public void LeftClickDown()
    {
        if (target != null)
        {
            print("Clicked on " + target.name + ", ID = " + target.ID);
            uiController.SetFocus(target);
        }
        else
            uiController.ClearFocus();
    }

    public PvPAvatar GetTarget()
    {
        return target;
    }
}