﻿using UnityEngine;
using System.Collections;

public class TestHero1 : Hero
{
    public TestHero1()
    {
        heroName = "TestHero1";
        heroSpells[0] = Spells.BasicAttack;
        heroSpells[1] = Spells.Fireball;
        heroSpells[2] = Spells.Heal;
    }
}
