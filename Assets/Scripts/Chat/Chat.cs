﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Chat.
/// </summary>
public class Chat : MonoBehaviour
{

    public static Queue<string> chatHistory = new Queue<string>();
    public static List<string> clientNames = new List<string>();

    string currentMessage = string.Empty;

    public static string myChatName = "Name";
    public static NetworkView _nv;

    // SINGLETON
    private static Chat _instance;
    public static Chat instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Chat>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    // CHAT SOUNDS
    public AudioClip sfxConnect;
    public AudioClip sfxDisconnect;
    public AudioClip sfxMsgSend;
    public AudioClip sfxMsgRcv;

    void Awake()
    {
        _nv = networkView;

        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }
    }

    private void OnGUI()
    {
        if (!Network.isClient && !Network.isServer)
            return;

        GUILayout.Space(20);
        GUILayout.BeginHorizontal(GUILayout.Width(550));

        currentMessage = GUILayout.TextField(currentMessage);

        // SEND MESSAGE
        if ((Event.current.keyCode == KeyCode.Return) || GUILayout.Button("Send"))
        {

            SendChatMessage();
        }

        // DISCONNEcT
        if (GUILayout.Button("Leave"))
        {

            ChatDisconnect();
        }

        GUILayout.EndHorizontal();

        foreach (string c in chatHistory)
        {
            GUILayout.Label(c);
        }
    }

    void ChatDisconnect()
    {
        // let chat know i DC
        string discMsg = "[" + myChatName + " has disconnected]";
        networkView.RPC("ChatMessage", RPCMode.Others, new object[] { discMsg, false });
        ChatMessage(discMsg, true);
        networkView.RPC("ClientDisconnected", RPCMode.All, new object[] { myChatName });

        bool amServer = Network.isServer;
        // and disconnect
        NetworkManager.RequestDisconnect(delegate
        {
            DisconnectHandling(amServer);
        });
    }

    void DisconnectHandling(bool isServer)
    {
        // CLIENT DC
        if (!isServer)
        {
            // send me to find menu
            MainMenuController.RequestMenuChange(MenuState.FIND);
        }
        // SERVER DC
        else
        {
            // send me to find menu
            MainMenuController.RequestMenuChange(MenuState.HOST);
        }

        // and clear chat history
        chatHistory.Clear();
    }

    void SendChatMessage()
    {
        if (!string.IsNullOrEmpty(currentMessage.Trim()))
        {
            myChatName = Network.isClient ? "Player 2" : "Player 1";
            currentMessage = myChatName + ": " + currentMessage;
            //networkView.RPC("ChatMessage", RPCMode.Others, new object[] { currentMessage, false });
            networkView.RPC("ChatMessage", RPCMode.Others, new object[] { currentMessage, false });
            ChatMessage(currentMessage, true);
            currentMessage = string.Empty;

            audio.PlayOneShot(sfxMsgSend);
        }
    }

    [RPC]
    // so ugly
    public void ChatMessage(string message, bool mySend)
    {
        chatHistory.Enqueue(message);

        if (!mySend)
            instance.audio.PlayOneShot(instance.sfxMsgRcv);

        if (chatHistory.Count > 21)
        {
            chatHistory.Dequeue();
        }
    }

    [RPC]
    public void ClientConnected(string clientName)
    {
        clientNames.Add(clientName);
    }

    [RPC]
    public void ClientDisconnected(string clientName)
    {
        clientNames.Remove(clientName);

        audio.PlayOneShot(sfxDisconnect);
    }

    public static string PrintChatClients()
    {
        string outStr = string.Empty;

        for (int i = 0; i < clientNames.Count; i++)
        {
            outStr += clientNames[i];

            // do i prints comma
            if (i < clientNames.Count - 1)
            {
                outStr += ", ";
            }
        }

        return outStr;
    }
}