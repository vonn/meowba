﻿using System.Collections.Generic;
using System;

/// <summary>
/// A list that can fire events when its contents are changed.
/// </summary>

[Serializable]
public class ObservedList<T> : List<T>
{
	/// <summary>
	/// When an existing item in the list is changed
	/// </summary>
	public event Action<int> Changed = delegate { };
	public event Action Updated = delegate { };
	public event Action<T> Added = delegate { };
	public event Action<T> Removed = delegate { };

	public new void Add(T item)
	{
		base.Add(item);
		Added(item);
	}

	public new void Remove(T item)
	{
		base.Remove(item);
		Removed(item);
	}

	public new void AddRange(IEnumerable<T> collection)
	{
		base.AddRange(collection);
		Updated();
	}

	public new void RemoveRange(int index, int count)
	{
		base.RemoveRange(index, count);
		Updated();
	}

	public new void Clear()
	{
		base.Clear();
		Updated();
	}

	public new void Insert(int index, T item)
	{
		base.Insert(index, item);
		Updated();
	}

	public new void InsertRange(int index, IEnumerable<T> collection)
	{
		base.InsertRange(index, collection);
		Updated();
	}

	public new void RemoveAll(Predicate<T> match)
	{
		base.RemoveAll(match);
		Updated();
	}
	
	
	public new T this[int index]
	{
		get
		{
			return base[index];
		}
		set
		{
			base[index] = value;
			Changed(index);
		}
	}
	
	
}