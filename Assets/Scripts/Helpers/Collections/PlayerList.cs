﻿using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// The list of player-controlled PvPAvatars in a game
/// </summary>
public class PlayerList : ObservedList<PlayerController> {

	// Constructor
	public PlayerList() {
		Added += pc => PlayerAdded(pc);
	}

	void PlayerAdded(PlayerController pc) {
		
	}
}
