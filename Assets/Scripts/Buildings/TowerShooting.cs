﻿using UnityEngine;
using System.Collections;

public class TowerShooting : MonoBehaviour {

	public GameObject projectile;

	// Use this for initialization
	void Start () {
		Instantiate (projectile, transform.position, new Quaternion());
		
	}
}
