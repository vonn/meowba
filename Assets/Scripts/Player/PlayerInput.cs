﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerController))]
public class PlayerInput : MonoBehaviour
{
    // which player do i belong to?
    PlayerController _pc;
    HoverSelectionController _hsc;
    float camRayLength = 100f;

    // Use this for initialization
    void Start()
    {
        _pc = GetComponent<PlayerController>();
        _hsc = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<HoverSelectionController>();
    }

    // Update is called once per frame
    void Update()
    {
        /*if (networkView.isMine) {*/
        TakeInput();
        /*}
        else {
            enabled = false;
        }*/
    }

    /// <summary>
    /// Takes user input
    /// </summary>
    void TakeInput()
    {
        // Right click on world
        if (Input.GetMouseButtonDown(1))
        {
            PvPAvatar clickedAv = _hsc.GetTarget();

            bool shiftDown = Input.GetKey(KeyCode.LeftShift);
            _pc.RightClickDown(shiftDown, camRayLength, clickedAv);
        }

        // Left click in world
        if (Input.GetMouseButtonDown(0))
        {
            _hsc.LeftClickDown();
        }

        // TODO get input for spells, etc.
    }
}
