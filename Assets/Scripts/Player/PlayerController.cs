﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerInput))]
/// <summary>
/// Defines a player character's PvPAvatar.
/// </summary>
public class PlayerController : PvPAvatar
{
    #region MEMBER VARS
    PlayerInput _in;

    /// <summary>
    /// The hero assigned to this PCtrl
    /// </summary>
    [SerializeField]
    public Hero hero = new TestHero1();
    [SerializeField]
    public bool isHero
    {
        get
        {
            return hero == null;
        }
    }

    public float respawnCD = 3f;
	public float respawnTime;
    #endregion

    void Awake()
    {
        base.Awake();
        _in = GetComponent<PlayerInput>();
        _type = AvatarType.PLAYER;
    }

    // Update is called once per frame
    void Update()
    {
        // Player dead
        if (_state == AvatarState.DEAD)
        {
            // ARISE, CHICKEN. CHICKEN, ARISE.
            if (respawnTime <= Time.time)
                Respawn();
            return;
        }
    }

    //protected override void LateUpdate()
    //{
    //    base.LateUpdate();
    //    // TODO my own lateupdate
    //}

    #region Input Events
    /// <summary>
    /// Rights the click down.
    /// </summary>
    /// <param name="attackMoveModifier">If set to <c>true</c> attack move modifier.</param>
    public void RightClickDown(bool attackMoveModifier, float rayLength, PvPAvatar clickTarget)
    {
        if (_state == AvatarState.DEAD) return;


        // pressing shift also? -> attack move 
        lookForFight = attackMoveModifier;

        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit floorHit;

        // first -- did i right click on a PvPAV?
        if (clickTarget != null)
        {
            /*if (Network.isServer)
                SetTarget(clickTarget, true);
            else */
                networkView.RPC("SetTargetRPC", RPCMode.All, new object[] { clickTarget.ID, true });
        }
        else if (Physics.Raycast(camRay, out floorHit, rayLength, _floorMask))
        {
            string name = _trans.name;
            NetworkManager.PlayerMove(this, name, floorHit.point);
            //DropTarget();
            //SetDestination(floorHit.point);
        }
    }
    #endregion

    #region Combat Mechanics

    #endregion

    #region Life and Death
    public void Die(int killerID)
    {
        base.Die(killerID);

        // move me far far away
        _navAgent.enabled = false;
        _trans.position = new Vector3(1000, 1000, 1000);

        // TODO award my killer the kill

        //set respawn timer
        respawnTime = Time.time + respawnCD;
    }
    #endregion

    #region Network
    //void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    //{
    //    int health = 0;

    //    // Write 
    //    if (stream.isWriting)
    //    {
    //        health = currentHealth;
    //        stream.Serialize(ref health);
    //    }
    //    // Read
    //    else
    //    {
    //        stream.Serialize(ref health);
    //        currentHealth = health;
    //    }
    //}
    #endregion
}
