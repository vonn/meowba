﻿using UnityEngine;
using System.Collections;

public class SeekTarget : MonoBehaviour {
	public int timeToReachMaxSpeed;
	public float startSpeed;
	public float maxSpeed;
	public PvPAvatar target;

	Rigidbody rigidbody;

	int time = 0;
	float speed;
	float speedIncrement;

	void Awake ()
	{
		rigidbody = GetComponent<Rigidbody> ();
		speed = startSpeed;
		speedIncrement = (maxSpeed - startSpeed) / timeToReachMaxSpeed;
	}

	void FixedUpdate() {
		if (time < timeToReachMaxSpeed)
		{
			time++;
			speed += speedIncrement;
		}

		Vector3 movement = target.transform.position - rigidbody.position; //vector from me to target
		movement = movement.normalized * speed * Time.deltaTime; //at the given speed

		Vector3 newPosition = rigidbody.position + movement; 
		rigidbody.MovePosition(newPosition); //move without regard to collision
	}
}
