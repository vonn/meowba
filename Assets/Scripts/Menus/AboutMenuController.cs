﻿using UnityEngine;
using System.Collections;

public class AboutMenuController : MonoBehaviour, MenuController {

    const string BACK = "BACK";

	public void HandleButton (string btnName)
    {
        switch (btnName) {
            case BACK:
                Application.LoadLevel(Scenes.MainMenu);
                break;
        }
    }
}
