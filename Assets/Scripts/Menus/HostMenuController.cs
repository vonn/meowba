﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HostMenuController : MonoBehaviour, MenuController {
    // Reference buttons with these const strings
    const string START = "START";
    const string BACK = "BACK";

    static InputField nameField;
    [SerializeField] Button btnBack;
    [SerializeField] Button btnStart;

    void Awake() {
        nameField = GameObject.Find("inputGameName").GetComponent<InputField>();
        btnBack = GameObject.Find("btnBack").GetComponent<Button>();
        btnStart = GameObject.Find("btnStart").GetComponent<Button>();

        SetupHandlers();
    }

    void SetupHandlers() {

        btnBack.onClick.AddListener(() => { 
            btn_Back();
        });

        btnStart.onClick.AddListener(() => { 
            btn_Start();
        });
    }

    void OnGUI() {
        if (Event.current.keyCode == KeyCode.Return) {
            //btn_Start();
        }
    }

    public void HandleButton (string btnName)
    {
        switch (btnName) {
            case START:
                btn_Start();
                break;
            case BACK:
                btn_Back();
                break;
        }
    }

    // When start host button pressed
    public void btn_Start() {
        string gameName = nameField.text.ToString().Trim();

        // Only non-empty strings
        if (!string.IsNullOrEmpty(gameName)) {
            NetworkManager.StartServer(gameName);
        }
    }

    public void btn_Back() {
        MainMenuController.RequestMenuChange(MenuState.MAIN);
    }
}
