﻿using UnityEngine;
using System.Collections;

public interface MenuController {

    /// <summary>
    /// Handles the button.
    /// </summary>
    /// <param name="btnName">Button name.</param>
	void HandleButton(string btnName);
}
