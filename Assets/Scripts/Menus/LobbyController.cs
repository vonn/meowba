﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public class LobbyController : MonoBehaviour, MenuController
{
    // Reference buttons with these const strings
    const string START = "START";

    public Button startBtn;

    // SINGLETON
    private static LobbyController _instance;
    public static LobbyController instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<LobbyController>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    public AudioClip readySFX;
    public AudioClip startingVFX;
    public AudioClip blip;

    void Awake()
    {
        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }

        //SetupHandlers();
    }

    void Update()
    {

    }

    void OnLevelWasLoaded(int level)
    {
        if (Application.loadedLevelName == Scenes.Lobby)
        {
            //SetupHandlers();

            //// reenable chat
            //if (!Chat.instance.gameObject.activeSelf)
            //{
            //    Chat.instance.gameObject.SetActive(true);
            //}

            // reenable the start button when we come back in here
            startBtn.interactable = true;

            // We shouldn't be here if we aren't connected
            if (!Network.isClient && !Network.isServer)
            {
                print("Lobby but not connected to a game! Sending back to find game.");
                MainMenuController.RequestMenuChange(MenuState.FIND);
                return;
            }
        }
    }

    void OnGUI()
    {
        if (NetworkManager.readyToStart)
        {

        }
    }

    //void SetupHandlers()
    //{
    //    if (Application.loadedLevelName != Scenes.Lobby) return;

    //    // retrieve the button
    //    startBtn = GameObject.Find("MenuCanvas").transform.FindChild("btnStart").GetComponent<Button>();

    //    startBtn.onClick.RemoveAllListeners();

    //    // click listeners
    //    startBtn.onClick.AddListener(() =>
    //    {
    //        HandleButton(START);
    //    });
    //}

    public void HandleButton(string btnName)
    {
        switch (btnName)
        {
            case START:
                // Let everyone know the game is starting

                networkView.RPC("StartingGame", RPCMode.All, new object[] { });

                break;
        }
    }

    [RPC]
    public IEnumerator StartingGame()
    {
        // diable start button
        startBtn.interactable = false;

        audio.PlayOneShot(startingVFX);
        yield return new WaitForSeconds(1);

        for (int i = 0; i < 3; i++)
        {

            audio.PlayOneShot(blip);
            yield return new WaitForSeconds(1);
        }

        yield return new WaitForSeconds(1);
        NetworkManager.instance.LoadGame();
    }

    [RPC]
    public void rpcReady()
    {
        OnReadyToStart();
    }

    [RPC]
    public void rpcNotReady()
    {
        OnNotReadyToStart();
    }

    public static void OnReadyToStart()
    {
        print("READY TO START");
        instance.audio.PlayOneShot(instance.readySFX);
        var mc = GameObject.Find("MenuCanvas");
        mc.transform.FindChild("lblReady").gameObject.SetActive(true);

        if (Network.isServer)
        {
            mc.transform.FindChild("btnStart").gameObject.SetActive(true);
            instance.networkView.RPC("rpcReady", RPCMode.Others, new object[] { });
        }
    }

    [RPC]
    public static void OnNotReadyToStart()
    {
        print("NOT READY TO START");
        var mc = GameObject.Find("MenuCanvas");
        mc.transform.FindChild("lblReady").gameObject.SetActive(false);

        if (Network.isServer)
        {
            mc.transform.FindChild("btnStart").gameObject.SetActive(false);
            instance.networkView.RPC("rpcNotReady", RPCMode.Others, new object[] { });
        }
    }
}
